Name:    iniparser
Version: 4.2.6
Release: 1
Summary: ini file parser 
License: MIT and Zlib
URL:	 https://gitlab.com/iniparser/iniparser
Source0: https://gitlab.com/iniparser/iniparser/-/archive/v%{version}/iniparser-v%{version}.tar.bz2

BuildRequires: 	gcc doxygen cmake

%description
This modules offers parsing of ini files from the C level. See a complete documentation in HTML format, from this directory open the file html/index.html with any HTML-capable browser.

%prep
%autosetup -n %{name}-v%{version} -p1

%build
%cmake -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=ON
%cmake_build

%install
%cmake_install
rm -rf %{buildroot}%{_bindir}/testrun
rm -rf %{buildroot}%{_bindir}/ressources
rm -rf %{buildroot}%{_docdir}/%{name}/examples

%files
%license LICENSE
%doc README.md INSTALL AUTHORS FAQ-en.md FAQ-zhcn.md  
%{_includedir}/*
%{_libdir}/*
%{_docdir}/*

%changelog
* Fri Feb 21 2025 Ge Wang <wang__ge@126.com> - 4.2.6-1
- Update to 4.2.6:
  * Fix heap overflow in iniparser_dumpsection_ini()(CVE-2025-0633)
  * Add test to reproduce heap overflow in iniparser_dumpsection_ini()
  * Fix links to FAQ sites in doxygen generated documentation
  * Add packaging status badge to give users and me an overview
  * Enable command line arguments for tests

* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 4.2.5-1
- update to 4.2.5

* Tue Dec 24 2024 xu_ping <707078654@qq.com> - 4.2.4-1
- Upgrade version to 4.2.4
  * Add null check in function dictionary_get()
  * Remove unused defines
  * Replace POSIX ssize_t with C standard size_t 
  * Add basic iniparser.pc
  * Support for closing square brackets within section
  * Support for escaped quotes in quoted values
  * Support for loading string/custom streams
  * Support for uin64_t
  * Update doxygen docs

* Sun Jun 25 2023 wangkai <13474090681@163.com> - 4.1-5
- Fix CVE-2023-33461

* Mon Aug 22 2022 yaoxin <yaoxin20@h-partners.com> - 4.1-4
- Fix not striped problem
- Remove rpath

* Tue Jan 19 2021 Ge Wang <wangge20@huawei.com> - 4.1-3
- Modify license information.

* Tue Oct 13 2020 liqingqing_1229 <liqingqing3@huawei.com>
- update source0

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com>
- Package init

